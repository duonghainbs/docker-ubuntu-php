#!/bin/bash

while getopts "u:p:" flag
do
  case "${flag}" in
    u)
        ubuntu="${OPTARG}"
        ;;
    p)
        php="${OPTARG}"
        ;;
  esac
done

if [[ -z "${ubuntu}" ]]; then
  echo "The variable 'ubuntu' does not exist."
  exit 1
elif [[ ! -d "${ubuntu}" ]]; then
  echo "The directory ${ubuntu} does not exist."
  exit 1
fi

if [[ -z "${php}" ]]; then
  echo "The variable 'php' does not exist."
  exit 1
elif [[ ! -d "${ubuntu}/${php}" ]]; then
  echo "The directory ${php} does not exist."
  exit 1
fi

tag=$ubuntu-$php
path=$ubuntu/$php

docker buildx build -t registry.gitlab.com/duonghainbs/docker-ubuntu-php:$tag $path
docker push registry.gitlab.com/duonghainbs/docker-ubuntu-php:$tag

printf '%s\n%s\n' "[$(date '+%d/%m/%Y %H:%M:%S')] BUILD: $tag" "$(cat _bash/build.log)" > _bash/build.log